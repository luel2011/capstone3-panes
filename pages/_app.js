import React, { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';
import { Container, Button } from 'react-bootstrap';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { UserProvider } from '../UserContext.js';
import AppHelper from '../app-helper';

export default function MyApp({ Component, pageProps }) {



	//global user state
	const [user, setUser] = useState({
		//user state is an object with properties from our local storage
		id: null,
		isAdmin: null,
		balance: null,
		 //convert string to boolean
	})

	//function to clear local storage upon logout
	const unsetUser = () => {
		localStorage.clear();

		setUser({
			id: null,
			isAdmin: null,
			balance: null,
		})
	}

	useEffect(() => {

		const options = {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() } ` }
		}

		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			if( typeof data._id !== undefined ) {
				setUser({ id: data._id, isAdmin: data.isAdmin, balance: data.balance })
			} else {
				setUser({ id: null, isAdmin: null, balance: null })
			}
		})
		
	}, [user.id])

	useEffect(() => {
        console.log(`User with id: ${user.id} is an admin: ${user.isAdmin}, with balance of ${user.balance}`);
    }, [ user.isAdmin, user.id, user.balance ]);

  	return (
  		<React.Fragment>
  			<UserProvider value={{ user, setUser, unsetUser }}>
		  		<NavBar />
		  		<Container>
					<Component {...pageProps} />
				
				</Container>
			</UserProvider>
		</React.Fragment>
	)
}