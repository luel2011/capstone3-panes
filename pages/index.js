import React from 'react';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Banner from '../components/Banner';


export default function Home() {

	const data = {
		title: "Let's BUDGET UP!",
		content: "Wherever your heart is, there you will find your treasure",
		destination: "/",
		label: ""
	}

	return (
		<React.Fragment>
			<Banner data={data}/>
		
		</React.Fragment>
	)
}