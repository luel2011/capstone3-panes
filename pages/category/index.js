import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';


export default function index() {

	const { user } = useContext(UserContext);
	

	const [ name, setName ] = useState('');
	const [ type, setType ] = useState('');
	const [isActive, setIsActive] = useState(false);


	function addCategory() {

		const options ={
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"name": name,
				"type": type,
				"userId": user.id

			})

		}

		fetch(`${ AppHelper.API_URL }/categories`, options)
		.then(AppHelper.toJSON)
		.then(data =>{
			console.log(data)

			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
			}

			Swal.fire('Category Successfully Added!')
		})
 
   
	}

	useEffect(() => {
		if(name !== ''  && type !== '' ){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[ name, type ]);


	return (
		<React.Fragment>
			<Container>
				<Row>
					<Col>
					</Col>
					<Col>
						<Form>
						  <Form.Group>
						    <Form.Label>Category Name:</Form.Label>
						    <Form.Control 
						    	type="text" 
						    	placeholder="Enter Category Name..." 
						    	value={name}
						    	onChange={e => setName(e.target.value)}
						    	required
						    />
						  </Form.Group>

						  <Form.Group>
						    <Form.Label>Transaction Type:</Form.Label>
						    <Form.Control
						    	as="select"
						    	value={type}
						    	onChange={e => setType(e.target.value)}
						    	required
						    >
						      <option>Income</option>
						      <option>Expense</option>
						    </Form.Control>
						  </Form.Group>

						    <Button
							  	block
							  	variant="outline-dark"
								// type="submit"
								id="submitBtn"
								onClick={() => addCategory()}

							>
							  	Add Category
							</Button>

						

						</Form>
						</Col>

						<Col>
						</Col>
				</Row>
			</Container>

		</React.Fragment>
	)
}