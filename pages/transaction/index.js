import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';
import { Form, Button, Row, Col } from 'react-bootstrap';
// import Balance from "../transaction/components/Balance";
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';
import { Jumbotron } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Swal from 'sweetalert2';


export default function index() {



	const { user, setUser } = useContext(UserContext);

	const [ userId, setUserId ] = useState('user')
	const [ type, setType ] = useState('Income');
	const [ name, setName ] = useState('');
	const [ createdOn, setCreatedOn ] = useState('');
	const [ categories, setCategories ] = useState([]);
	const [ category, setCategory ] = useState('');
	const [ amount, setAmount ] = useState('');
	const [isActive, setIsActive] = useState(false);





	useEffect(() =>{
		const options ={
			method: 'GET', 
			headers: {
				'Content-Type': 'application/json'
			}

		}
		fetch(`${ AppHelper.API_URL }/categories`, options)
		.then(AppHelper.toJSON)
		.then(data =>{

			const tempCategories = data.filter(category => {
				if(type === category.type) {
					return true
				}
			})

			const categoryOptions = tempCategories.map(category => {
				return(
					<option>{category.name}</option>

				)
			}) 

			console.log(categoryOptions)
			setCategories(categoryOptions);
		})

	}, [ type ])

		
	function addTransaction() {

		console.log(category)
		console.log(typeof amount)
		const options ={
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"name": name,
				"type": type,
				"category": category,
				"amount": parseFloat(amount),
				"userId": user.id

			})

		}
		fetch(`${ AppHelper.API_URL }/transactions`, options)
		.then(AppHelper.toJSON)
		.then(data =>{
			console.log(data)

			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
			}


			Swal.fire('Transaction Successfully Added!')

		})
		
		
	}

	useEffect(() => {
		if(name !== ''  && category !== '' && amount !== '' ){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[ name, category, amount]);



	return(

		<>

		<Jumbotron variant="dark">
			<h2>Total Remaining Balance: ${user.balance}</h2>
		</Jumbotron>
		<Row>
			<Col>
			</Col>
			
			<Col>
				<Form variant="outline-dark">

					<Form.Group>

						<Form.Label>Transaction Type:</Form.Label>
						    <Form.Control
						    	as="select"
						    	value={type}
						    	onChange={e => setType(e.target.value)}
						    	required
						    >
						      <option>Income</option>
						      <option>Expense</option>
						    </Form.Control>
						</Form.Group>

						<Form.Group>
						    <Form.Label>Category Name:</Form.Label>
						    <Form.Control
						    	as="select"
						    	value={category}
						    	onChange={e => setCategory(e.target.value)}
						    	required
						    >
						    <option>Select Category</option>
						      {categories}
						    </Form.Control>
						 </Form.Group>

						
					   <Form.Group>
					    	<Form.Label>Transaction Name: </Form.Label>
						    <Form.Control 
						    	type="text"
						    	value={name}
						    	placeholder="Enter Transaction Name..."
						    	onChange={e => setName(e.target.value)}
						    	required
						    />
					  	</Form.Group>

					  	 <Form.Group>
					    	<Form.Label>Transaction Date: </Form.Label>
						    <Form.Control 
						    	selected={createdOn}
						    	type="date"
						    	onChange={e => setCreatedOn(e.target.value)}
						    	required

						    />
					  	</Form.Group>

						<Form.Group>
						   	<Form.Label>Transaction Amount: </Form.Label>
						    <Form.Control 
						    	type="number"
						    	value={amount}
						    	placeholder="Enter Transaction Amount..."
						    	onChange={e => setAmount(e.target.value)}
						    	required
						    />
						 </Form.Group>
						   <Button
							  	block
							  	variant="outline-dark"
								// type="submit"
								id="submitBtn"
								onClick={(notify) => addTransaction()}

							>
							  	Add Transaction
							</Button>

				  

				</Form>
			</Col>

			<Col>
			</Col>
		</Row>
		</>
	)
};