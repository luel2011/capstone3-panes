import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';
import { Row, Col, Jumbotron, ListGroup, Container, Button} from 'react-bootstrap';
import IncomeDoughnut from '../../components/IncomeDoughnut';
import Linechart from '../../components/LineChart';


export default function Charts() {
	return (
		<>
			<IncomeDoughnut/>
			<Linechart/>

		</>

	)
}