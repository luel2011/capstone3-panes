import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';
import { Row, Col, Jumbotron, ListGroup, Container, Button} from 'react-bootstrap';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';
import IncomeList from '../../components/IncomeList';
import ExpenseList from '../../components/ExpenseList';
import SearchForm from '../../components/SearchForm';




export default function index() {

	const { user, setUser } = useContext(UserContext);

	const [ filterTransaction, setFilterTransaction ] = useState('')



	return(

		<>
			<Jumbotron>
				<h4> Total Remaining Balance: ${user.balance} </h4> 

			</Jumbotron>


			<SearchForm/>
			<Container>

				<Row className="mb-5">
					<Col>
						<Button 
						  	variant="outline-dark" 
						  	type="submit" 
						  	block
						  	onClick={e => setFilterTransaction("All")}
						  	>
						    	All
						  </Button>
					</Col>
					<Col>
						  <Button 
						  	variant="outline-dark" 
						  	type="submit" 
						  	block
						  	onClick={e => setFilterTransaction("Income")}

						  	>
						    	Income
						  </Button>
					</Col>
					<Col>
						  <Button 
						  	variant="outline-dark" 
						  	type="submit" 
						  	block
						  	onClick={e => setFilterTransaction("Expense")}

						  	>
						    	Expense
						  </Button>
					</Col>
				</Row>

				{(filterTransaction === "All")
					?
					<Row>
						<Col>
							<IncomeList/>
						</Col>

						<Col>
							<ExpenseList/>
						</Col>

					</Row>

					:
					(filterTransaction === "Income")
					?
					<Row>
						<Col>
							<IncomeList/>
						</Col>
					</Row>

					:
					(filterTransaction === "Expense")

					?
					<Row>
						<Col>
							<ExpenseList/>
						</Col>

					</Row>
					:null
				} 
					


			</Container>
		</>
	)
}