import { useState, useEffect } from 'react';
import Router from 'next/router';
import { Form, Button } from 'react-bootstrap';
import AppHelper from '../../app-helper';

export default function index() {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	const emailExist = (response) => {
		console.log(response)

		const options ={
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"email": email
			})
		}
		fetch(`${ AppHelper.API_URL }/users/email-exists`, options)
		.then(AppHelper.toJSON)
		.then(data =>{
			console.log(data)

			if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } 

		})


	}

	function registerUser(e) {
		e.preventDefault();

		const options ={
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"name": name,
				"email": email,
				"password": password1

			})

		}

		fetch(`${ AppHelper.API_URL }/users`, options)
		.then(AppHelper.toJSON)
		.then(data =>{
			console.log(data)

			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
			}


		})

		alert('Thank you for registering!');
		Router.push('/login')
	}
	// form => registerUser()

	useEffect(() => {
		if((name !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[ name, email, password1, password2]);

	return (
		<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">

			<Form.Group>
				<Form.Label>Name:</Form.Label>
				<Form.Control 
					type="name"
					placeholder="Kimi no Nawa"
					value={name}
					onChange={e => setName(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive
				?
				<Button block
					variant="outline-dark"
					type="submit"
					id="submitBtn"
				>
					Submit
				</Button>
				:
				<Button block
					className="bg-secondary"
					type="submit"
					id="submitBtn"
					disabled
				>
					Submit
				</Button>
			}
	
		</Form>
	)
};