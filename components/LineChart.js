import React, { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../app-helper';
import { Line } from 'react-chartjs-2';
import UserContext from '../UserContext';





export default function index({amount}) {

	const { user, setUser } = useContext(UserContext);


	const [ incomeAmount, setIncomeAmount ] = useState(0);
	const [ expenseAmount, setExpenseAmount ] = useState(0);
	const [ totalIncome, setTotalIncome ] = useState('');
	const [ totalExpense, setTotalExpense ] = useState('');
	

	useEffect(() =>{
		const options ={
			method: 'GET', 
			headers: {
				'Content-Type': 'application/json'
			}

		}
		fetch(`${ AppHelper.API_URL }/transactions/user/${user.id}`, options)
		.then(AppHelper.toJSON)
		.then(data =>{

			let tempIncome = 0
			let tempExpense = 0
			let indivIncome = []
			let indivExpense = []

			const incomeTransactions = data.filter(transaction => {
				if(transaction.type === "Income") {
					return true
				}
				
				

			})


			incomeTransactions.forEach(transaction => {
				indivIncome.push(transaction.amount)

				// transaction = JSON.parse(transaction)
				tempIncome += (transaction.amount)
			})
			

			const expenseTransactions = data.filter(transaction => {
				if(transaction.type === "Expense") {
					return true
				}
				indivExpense.push(transaction)
			})


			expenseTransactions.forEach(transaction => {
			
				tempExpense += (transaction.amount)

				tempExpense * -1

			})


			setIncomeAmount(tempIncome);
			setExpenseAmount(tempExpense);
			setTotalIncome(indivIncome);
			setTotalExpense(indivExpense);


		})

	})


	return(

		<>

				<Line
			        data={{
			          datasets:[{
			            data: totalIncome,
			            backgroundColor: "gainsboro",
			            label: "Income"
			        },
			        {
			        	data: totalExpense,
			            backgroundColor: "dimgrey",
			            label: "Expense"
			        }


			        ],
			          labels:["1", "2"]
			        }}
			        redraw = {false}

			    />   
	
	    </>
	)
}