import React, { useContext } from 'react';
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function NavBar() {

    const { user } = useContext(UserContext);

    return (
        <Navbar expand="lg" bg="dark" variant="dark">
            <Link href="/">
                <a className="navbar-brand">BUDGET UP!!</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav justify variant="tabs" defaultActiveKey="/home">
                    {(user.id !== undefined)
                            ? 
                            <React.Fragment>
                                <Link href="/category">
                                    <a  className="nav-link" role="button">Category</a>
                                </Link>
                                <Link href="/transaction">
                                    <a className="nav-link" role="button">Transactions</a>
                                </Link>
                                <Link href="profile">
                                    <a className="nav-link" role="button">History</a>
                                </Link>
                                 <Link href="/stats">
                                    <a className="nav-link" role="button">Stats</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                                </Link>
                               
                            </React.Fragment>
                            
                        : 
                        <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Sign Up!</a>
                            </Link>
                        </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
