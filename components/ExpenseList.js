import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../app-helper';
import { Jumbotron, Table } from 'react-bootstrap';
import UserContext from '../UserContext';





export default function ExpenseList() {

	const { user, setUser } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ amount, setAmount ] = useState('');
	const [ transactions, setTransactions] = useState([])
	const [ type, setType ] = useState('');
	const [ category, setCategory ] = useState('');
	const [isActive, setIsActive] = useState(false);



	useEffect(() =>{
		const options ={
			method: 'GET', 
			headers: {
				'Content-Type': 'application/json'
			}

		}
		fetch(`${ AppHelper.API_URL }/transactions/user/${user.id}`, options)
		.then(AppHelper.toJSON)
		.then(data =>{

			// filtering the transactions base on transaction type
			const expenseTransactions = data.filter(transaction => {
				if(transaction.type === "Expense") {
					return true
				}
			})


			const expenseList = expenseTransactions.map(transactions => {
				return (
					<>
					<tr>
					  <td>{transactions.name}</td>
					  <td>{transactions.type}</td>
					  <td>{transactions.amount}</td>
					  <td>{transactions.createdOn}</td>
					  <td>
					  	<Row>
					  		<Col>
					  			<Button 
					  				variant="outline-light" 
					  				size="sm"
					  			>
					  				Delete Transaction
					  			</Button>
					  		</Col>
					  		<Col>
					  			<Button 
					  				variant="outline-light" 
					  				size="sm"
					  			>
					  				Update Transaction
					  			</Button>
					  		</Col>
					  	</Row>
					  </td>
					</tr>

					 
					 </>
					
				)
			}) 

			setTransactions(expenseList)

		})



	})




	return(

		<>

			<h4>Transaction History(Expense)</h4>
			<Table striped bordered hover variant="dark">

			  <thead>
			    <tr>
			      <th>Transaction</th>
			      <th>Category</th>
			      <th>Amount</th>
			      <th>Date</th>
			      <th>Action</th>
			    </tr>
			  </thead>
			  <tbody>{transactions}</tbody>

			</Table>
		</>
	)
}