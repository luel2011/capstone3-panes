import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import SearchResult from './SearchResult';
import AppHelper from '../app-helper';
import UserContext from '../UserContext';



export default function SearchForm({searchTransactions}) {

	const { user, setUser } = useContext(UserContext);

	const [ target, setTarget ] = useState("")
	const [ all, setAll ] = useState([])
	const [ match , setMatch ]  =useState('')

	useEffect(() => {

		const options ={
			method: 'GET', 
			headers: {
				'Content-Type': 'application/json'
			}

		}
		fetch(`${ AppHelper.API_URL }/transactions/user/${user.id}`, options)
		.then(AppHelper.toJSON)
		.then(data =>{

			setAll(data)



		})


	})

	function search(e) {
		e.preventDefault()

		const matchItem = all.find(item => {
			return target === item.name 
		}) 

		setMatch(matchItem)
	}


	return (
		<>
		<Form onSubmit={e =>search(e)}>
		  <Form.Group controlId="formBasicCategory">
		    <Form.Label>Search</Form.Label>
		    <Form.Control onChange={e => setTarget(e.target.value)} value={target} type="text" placeholder="Search" />
		  </Form.Group>

		  <Button 
		  	variant="outline-dark" 
		  	type="submit" 
		  	block

		  	>
		    	Search
		  </Button>
		</Form>

		<SearchResult value={match}/>


		</>

	)
}