import React, { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../app-helper';
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import UserContext from '../UserContext';




export default function index({amount}) {

	const { user, setUser } = useContext(UserContext);


	const [ incomeAmount, setIncomeAmount ] = useState(0);
	const [ expenseAmount, setExpenseAmount ] = useState(0);
	const [ date, setDate ] = useState('')
	const [ type, setType ] = useState('');

	

	useEffect(() =>{
		const options ={
			method: 'GET', 
			headers: {
				'Content-Type': 'application/json'
			}

		}
		fetch(`${ AppHelper.API_URL }/transactions/user/${user.id}`, options)
		.then(AppHelper.toJSON)
		.then(data =>{

			let tempIncome = 0
			let tempExpense = 0
			let tempDate = []

			const incomeTransactions = data.filter(transaction => {
				if(transaction.type === "Income") {
					return true
				}
			})


			incomeTransactions.forEach(transaction => {
				// transaction = JSON.parse(transaction)
				tempIncome += JSON.parse(transaction.amount)

			})
			

			const expenseTransactions = data.filter(transaction => {
				if(transaction.type === "Expense") {
					return true
				}
			})


			expenseTransactions.forEach(transaction => {
				// transaction = JSON.parse(transaction)
				tempExpense += JSON.parse(transaction.amount)

				tempExpense * -1


			})

			
			



			setIncomeAmount(tempIncome);
			setExpenseAmount(tempExpense);


		})

	})


	return(

		<>
		<Row>
			<Col>
				<Doughnut
			        data={{
			          datasets:[{
			            data: [incomeAmount, expenseAmount],
			            backgroundColor: ["gainsboro", "dimgrey"]
			          }],
			          labels:["Income", "Expense"]
			        }}
			        redraw = {false}

			    />   
		    </Col>

		    <Col>
				<Bar
			        data={{
			          datasets:[{
			            data: [incomeAmount, expenseAmount],
			            backgroundColor: ["gainsboro", "dimgrey"]
			          }],
			          labels:["Income", "Expense"]
			        }}
			        redraw = {false}

			    />   
		    </Col>

		    <Col>
				<Line
			        data={{
			          datasets:[{
			            data: [incomeAmount, expenseAmount],
			            backgroundColor: "gainsboro"
			          }],
			          labels:[''],
			          options: {
			                  scales: {
			                      yAxes: [{
			                          stacked: true
			                      }]
			                  }
			              }
			        }}
			        redraw = {false}

			    />   
		    </Col>




	  	</Row>

	    </>
	)
}

