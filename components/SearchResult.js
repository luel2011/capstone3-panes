import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';
import { Form, Button, Row, Col, Table } from 'react-bootstrap';
import AppHelper from '../app-helper';
import { Jumbotron, ListGroup } from 'react-bootstrap';
import UserContext from '../UserContext';



export default function index({value}) {

	const { user, setUser } = useContext(UserContext);


	const [ item, setItem ] = useState('');
	


	useEffect(() =>{
		const itemRow =
		<Table variant="dark">
			<tr>
		      <td>{value.name}</td>
		      <td>{value.type}</td>
		      <td>{value.amount}</td>
		      <td>{value.createdOn}</td>
			</tr>
		</Table> 
			

		setItem(itemRow)

	 },[ value ])






	return(

		<>
			<h6>Search Result:</h6>
			{item}
		</>
	)
}